import Vue from 'vue';
import Router from 'vue-router';
import Movies from './views/Movies.vue';
import ShareMovie from './views/ShareMovie.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'movies',
      component: Movies,
    },
    {
      path: '/sharemovie',
      name: 'sharemovie',
      component: ShareMovie,
    },
  ],
});
