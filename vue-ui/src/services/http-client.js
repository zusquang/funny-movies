import axios from 'axios';
import localStorage from 'local-storage';

/** Default config for axios instance */
let config = {
  baseURL: 'http://localhost:3000/'
};

/** Creating the instance for axios */
const httpClient = axios.create(config);

/** Auth token interceptors */
const authInterceptor = config => {
  if (localStorage.get('token')) {
    config.headers['Authorization'] = `JWT ${localStorage.get('token')}`;  
  }
  return config;
};

/** Logger interceptors */
const loggerInterceptor = config => {
  return config;
}

/** Adding the request interceptors */
httpClient.interceptors.request.use(authInterceptor);
httpClient.interceptors.request.use(loggerInterceptor);

/** Adding the response interceptors */
httpClient.interceptors.response.use(
  response => {
    /** TODO: Add any response interceptors */
    return response;
  },
  error => {
    /** TODO: Do something with response error */
    return Promise.reject(error);
  }
);

export { httpClient };