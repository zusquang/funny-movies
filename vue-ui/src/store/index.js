import Vue from 'vue';
import Vuex from 'vuex';
import VueJwtDecode from 'vue-jwt-decode';

import uuidv4 from 'uuid/v4';

import { httpClient } from '@/services';

import localStorage from 'local-storage';

import actionTypes from './action-types';
import mutationTypes from './mutation-types';

const LOGIN_OR_REGISTER_API = 'contributor/loginOrRegister'
const LOAD_MOVIES = 'movie/list'
const SHARE_MOVIE = 'movie/share'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    contributor: {
      email: '',
      isLogged: false
    },
    movies: [],
  },
  getters: {
    moviesCount: state => state.movies.length,
  },
  mutations: {
    [mutationTypes.LOAD_CONTRIBUTOR](state, { token }) {
      const contributorTkn = VueJwtDecode.decode(token);
      state.contributor = {
        email: contributorTkn.email,
        isLogged: true
      };
    },
    [mutationTypes.LOAD_MOCK_CONTRIBUTOR](state, { token }) {
      state.contributor = {
        email: '',
        isLogged: false
      };
    },
    [mutationTypes.LOGIN_OR_REGISTER](state, { email, password }) {
      httpClient.post(LOGIN_OR_REGISTER_API, { email, password }).then((res) => {
        localStorage.set('token', res.data.token);
        state.contributor = {
          email,
          isLogged: true
        };
      });
    },
    [mutationTypes.LOGOUT](state) {
      localStorage.set('token', null);
      state.contributor = {
        email: '',
        isLogged: false
      };
    },
    [mutationTypes.SHARE_MOVIE](state, { link }) {
      httpClient.post(SHARE_MOVIE, { link }).then((res) => {
        state.movies.unshift(res.data.movie);
      });
    },
    [mutationTypes.LOAD_MOVIES_SCROLLING](state, { lastId }) {
      const LOAD_MOVIES_WITH_LASTID = `${LOAD_MOVIES}?lastId=${lastId}`;
      httpClient.get(LOAD_MOVIES_WITH_LASTID).then(res => {
        state.movies = state.movies.concat(res.data.movies);
      });
    },
    [mutationTypes.LOAD_MOVIES_DEFAULT](state, { lastId }) {
      httpClient.get(LOAD_MOVIES).then(res => {
        state.movies = res.data.movies;
      });
    },
  },
  actions: {
    [actionTypes.LOAD_CONTRIBUTOR_AND_MOVIES]({ commit }) {
      const token = localStorage.get('token') || '';
      const contributorTagetMutationType = token ? mutationTypes.LOAD_CONTRIBUTOR
        : mutationTypes.LOAD_MOCK_CONTRIBUTOR;
      const moviesTagetMutationType = mutationTypes.LOAD_MOVIES_DEFAULT;
      commit(contributorTagetMutationType, { token });
      commit(moviesTagetMutationType, { lastId: '' });
    },
    [actionTypes.LOAD_MOVIES]({ commit }, { lastId }) {
      const tagetMutationType = lastId ? mutationTypes.LOAD_MOVIES_SCROLLING
        : mutationTypes.LOAD_MOVIES_DEFAULT;
      commit(tagetMutationType, { lastId });
    },
    [actionTypes.LOGIN_OR_REGISTER]({ commit }, { email, password }) {
      const tagetMutationType = mutationTypes.LOGIN_OR_REGISTER;
      commit(tagetMutationType, { email, password });
    },
    [actionTypes.LOGOUT]({ commit }) {
      const tagetMutationType = mutationTypes.LOGOUT;
      commit(tagetMutationType);
    },
    [actionTypes.SHARE_MOVIE]({ commit }, { link }) {
      const tagetMutationType = mutationTypes.SHARE_MOVIE;
      commit(tagetMutationType, { link });
    },
  },
});

export default store;
