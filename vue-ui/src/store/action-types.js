const actionTypes = {
  LOAD_CONTRIBUTOR_AND_MOVIES: 'LOAD_CONTRIBUTOR_AND_MOVIES',
  LOAD_MOVIES: 'LOAD_MOVIES',
  LOGIN_OR_REGISTER: 'LOGIN_OR_REGISTER',
  LOGOUT: 'LOGOUT',
  SHARE_MOVIE: 'SHARE_MOVIE'
};

export default actionTypes;
