Vue.js with Vuex and Vue Router.

## Project Dependencies
- Node.js
- Yarn

## Development Environment Setup
1. Install Node.js
1. Install Yarn
1. Run `yarn install` to install packages
1. Run `yarn serve` to start developemnt server
