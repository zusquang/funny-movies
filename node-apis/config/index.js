const CONFIG = {
  development: {
    internal: {},
    cors: {
      host: [],
    },
    mongodb: {
      port: 27017,
      host: '13.250.97.228',
      db: 'ror',
      dbuser: '111',
      dbpass: '1qaz2wsx3edc!@#QAZ',
    },
  },
  testing: {
    internal: {},
    cors: {
      host: [],
    },
    mongodb: {
      port: 27017,
      host: '13.250.97.228',
      db: 'ror',
      dbuser: '111',
      dbpass: '1qaz2wsx3edc!@#QAZ',
    },
  },
  uat: {
    internal: {},
    cors: {
      host: [],
    },
    mongodb: {
      port: 27017,
      host: '13.250.97.228',
      db: 'ror',
      dbuser: '111',
      dbpass: '1qaz2wsx3edc!@#QAZ',
    },
  },
  production: {
    internal: {},
    cors: {
      host: [],
    },
    mongodb: {
      port: 27017,
      host: '13.250.97.228',
      db: 'ror',
      dbuser: '111',
      dbpass: '1qaz2wsx3edc!@#QAZ',
    },
  },
};

module.exports = env => CONFIG[env || 'development'];
