const config = require('./index')(process.env.NODE_ENV);

const HOST = config.mongodb.host;
const PORT = config.mongodb.port;
const DB = config.mongodb.db;

const DBUSER = config.mongodb.dbuser;
const DBPASS = config.mongodb.dbpass;

const DBSTR = `mongodb://${HOST}:${PORT}/${DB}?authSource=ror`;

module.exports = {
  'jwtsecret': 'funnymovies',
  'database': DBSTR,
  'dbuser': DBUSER,
  'dbpass': DBPASS,
};
