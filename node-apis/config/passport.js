var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

var Contributor = require('../models/contributor');
var config = require('../config/database');

module.exports = function(passport) {
  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
  opts.secretOrKey = config.jwtsecret;
  passport.use(new JwtStrategy(opts, function(payload, done) {
    Contributor.findOne({ email: payload.email }, function(err, contributor) {
          if (err) {
              return done(err, false);
          }
          if (contributor) {
              done(null, contributor);
          } else {
              done(null, false);
          }
      });
  }));
};
