const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const cors = require('cors');
const config = require('./config/database');

mongoose.connect(config.database, { useCreateIndex: true, useNewUrlParser: true, user: config.dbuser, pass: config.dbpass });

const contributors = require('./routes/contributors');
const movies = require('./routes/movies');

const app = express();

app.use(cors());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morgan('dev'));
app.use(passport.initialize());

app.get('/', function(req, res) {
  res.send('Yeah, the apis ready to use..........');
});

app.use('/contributor', contributors);
app.use('/movie', movies);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not found apis.....');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // not found
  res.status(err.status || 500);
  res.json({ status: err.status, message: err.message });
});

module.exports = app;
