const config = require('../config/database');
const jwt = require('jsonwebtoken');

const Contributor = require('../models/contributor');

const getToken = (id, email) => jwt.sign({ id, email }, config.jwtsecret, { expiresIn: 604800 });

module.exports = {
	findOne: email => Contributor.findOne({ email }),
	create: contributor => Contributor.create(contributor),
	getToken: (id, email) => getToken(id, email),
	comparePassword: (contributor, password) => {
		return new Promise((resolve, reject) => {
			contributor.comparePassword(password, function (err, isMatch) {
			  if (isMatch && !err) {
					resolve(getToken(contributor.id, contributor.email));
			  } else {
			  	resolve('');
			  }
			});
		});
	},
}
