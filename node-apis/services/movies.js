const config = require('../config/database');
const jwt = require('jsonwebtoken');
const ObjectID = require('bson-objectid');

const Movie = require('../models/movie');

module.exports = {
	getList: lastId => {
		return new Promise((resolve, reject) => {
			const query = [];
		  if (lastId) {
		    query.push({ $match: { _id: { $gt: ObjectID(lastId) } } });
		  }
		  query.push({
		    $lookup: {
		      from: 'contributors',
		      let: { id: '$contributorId' },
		      pipeline: [
		        {
		          $match: {
		            $expr: {
		              $and: [
		                { $eq: ['$id', '$$id'] },
		              ],
		            },
		          },
		        },
		        { $project: { _id: 0, id: 1, email: 1 } },
		      ],
		      as: 'contributor',
		    },
		  });
		  query.push({ $unwind: '$contributor' });
		  query.push({ $sort: { _id: 1 } });
		  query.push({ $limit: 11 });
		  Movie.aggregate(query).then(rs => resolve(rs));
		});
	},
	create: movie => Movie.create(movie),
}
