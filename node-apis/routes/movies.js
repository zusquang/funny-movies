const passport = require('passport');

require('../config/passport')(passport);

const uuidv4 = require('uuid/v4');
const config = require('../config/database');
const express = require('express');
const rq = require('request-promise');
const jwt = require('jsonwebtoken');

const router = express.Router();

const MoviesService = require('../services/movies');

router.post('/share', passport.authenticate('jwt', { session: false }), function(req, res) {
  if (!req.body.link) {
    return res.json({ success: false, msg: 'Please input link.' });
  }
  jwt.verify(getToken(req.headers), config.jwtsecret, function(err, decoded) {
    if (err) {
      return res.status(403).send({ success: false, msg: 'Unauthorized.' });   
    }
    // Fetch youtube data
    rq.get(`https://www.youtube.com/oembed?url=${req.body.link}&format=json`).then((fres) => {
      const youO = JSON.parse(fres);
      // Create a share movie.
      const movieId = uuidv4();
      const movie = {
        id: movieId,
        title: youO.title,
        description: `Youtube ${youO.title}`,
        link: req.body.link,
        contributorId: decoded.id,
        contributor: {
          id: decoded.id,
          email: decoded.email
        },
        type: 'youtube',
      };
      MoviesService.create(movie);
      res.json({ success: true, movie });  
    });
  });
});

router.get('/list', function(req, res) {
  MoviesService.getList(req.query.lastId).then(rs => res.json({ success: true, movies: rs }));
});

getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};

module.exports = router;
