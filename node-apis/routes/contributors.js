const passport = require('passport');

require('../config/passport')(passport);

const uuidv4 = require('uuid/v4');
const config = require('../config/database');
const express = require('express');
const jwt = require('jsonwebtoken');
const _ = require('lodash')

const router = express.Router();

const ContributorsService = require('../services/contributors');

router.post('/loginOrRegister', function(req, res) {
  if (!req.body.email || !req.body.password) {
    res.json({ success: false, msg: 'Please pass emai and password.' });
  } else {
  	ContributorsService.findOne(req.body.email).then((contributor) => {
		  if (_.isEmpty(contributor)) {
			// Create contributor & return token.
			const contributorId = uuidv4();
			ContributorsService.create({ id: contributorId, email: req.body.email, password: req.body.password });
		    res.json({ success: true, token: ContributorsService.getToken(contributorId, req.body.email) });
		  } else { 
		    // Check if password matches
		    ContributorsService.comparePassword(contributor, req.body.password).then((token) => {
		    	if (!token) {
		    		res.status(401).send({ success: false, msg: 'Authentication failed. Wrong password.' });
		    	}
		    	if (token) {
		    		res.json({ success: true, token });
		    	}
		    });
		  };
    })
  }
});

module.exports = router;
