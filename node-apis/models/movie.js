var mongoose = require('mongoose');
var Promise = require('bluebird');

var Schema = mongoose.Schema;
mongoose.Promise = Promise;

var MovieSchema = new Schema({
  id: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  contributorId: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  link: {
    type: String,
    required: true    
  },
  type: {
    type: String,
    enum: ['youtube', 'facebook'],
    required: true,
    default: 'youtube'
  }
});

module.exports = mongoose.model('Movie', MovieSchema);
