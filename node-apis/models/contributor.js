var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Promise = require('bluebird');

var Schema = mongoose.Schema;
mongoose.Promise = Promise;

var ContributorSchema = new Schema({
    id: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

ContributorSchema.pre('save', function (next) {
    var contributor = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) return next(err);
            bcrypt.hash(contributor.password, salt, null, function (err, hash) {
                if (err) return next(err);
                contributor.password = hash;
                next();
            });
        });
    } else {
        next();
    }
});

ContributorSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Contributor', ContributorSchema);
