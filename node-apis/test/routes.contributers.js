process.env.NODE_ENV = 'testing';

const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();

const config = require('../config/database');

chai.use(chaiHttp);

const TEST_CONTRIBUTOR = {
    id: '1b8227e2-fef8-43b4-a2b4-7e3838ae8d9a',
    email: 'testing1@gmail.com',
    password: '123456789'
};

describe('Contributors', () => {
    beforeEach((done) => {
        done();
    });
    /*
     * Test the /POST contributor login or register
     */
    describe('/POST contributor login or register', () => {
    	it('Register - it should not POST a contributor without email or empty email field', (done) => {
            let contributor = {
                email: 'testing1@gmail'
            };
            chai.request(server)
                .post('/contributor/loginOrRegister')
                .send(contributor)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('msg').eql('Please pass emai and password.');
                    done();
                });
        });
        it('Register - it should not POST a contributor without password or empty password field', (done) => {
            let contributor = {
                password: '123456789'
            };
            chai.request(server)
                .post('/contributor/loginOrRegister')
                .send(contributor)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('msg').eql('Please pass emai and password.');
                    done();
                });
        });
        it('Register - it should POST a contributor', (done) => {
        	let contributor = TEST_CONTRIBUTOR;
            chai.request(server)
                .post('/contributor/loginOrRegister')
                .send(contributor)
                .end((err, res) => {
                	const token = jwt.sign({ id: contributor.id, email: contributor.email }, config.jwtsecret, { expiresIn: 604800 });
                    res.should.have.status(200);
					res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('token').eql(token);
                    done();
                });
        });
        it('Login - it should not POST a login action of contributor if password is wrong', (done) => {
            let contributor = {
                email: 'testing1@gmail.com',
                password: '12345678900'
            };
            chai.request(server)
                .post('/contributor/loginOrRegister')
                .send(contributor)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('msg').eql('Authentication failed. Wrong password.');
                    done();
                });
        });
    });
});