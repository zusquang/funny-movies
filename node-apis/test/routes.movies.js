process.env.NODE_ENV = 'testing';

const jwt = require('jsonwebtoken');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();

const config = require('../config/database');

chai.use(chaiHttp);

const VALID_TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjQxNTc1MDhmLTk3NWEtNGJkNS04MDIxLTdkZGRiNzYwMTUwMSIsImVtYWlsIjoiZXJpY0BhZHZvLmNvbSIsImlhdCI6MTU3MDg1MTcwMiwiZXhwIjoxNTcxNDU2NTAyfQ.4U9rbTXmeegJdvCK6-wPJV767HgMVu9ceU4DKw5Nlts';
const INVALID_TOKIEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';

describe('Movies', () => {
    beforeEach((done) => {
        done();
    });
    /*
     * Test the /POST share a movie
     */
    describe('/POST share a movie', () => {
    	it('it should not POST a movie without link or empty link field', (done) => {
            let movie = {
                link: ''
            };
            chai.request(server)
                .post('/movie/share')
                .set({ 'Authorization': `JWT ${VALID_TOKEN}` })
                .send(movie)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(false);
                    res.body.should.have.property('msg').eql('Please input link.');
                    done();
                });
        });
        it('it should not POST a movie without contributor authenticated', (done) => {
            let movie = {
                link: 'https://www.youtube.com/watch?v=xAG1uF9O1Is'
            };
            chai.request(server)
                .post('/movie/share')
                .send(movie)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object').that.is.empty;
                    done();
                });
        });
        it('it should not POST a movie with a invalid form of contributor', (done) => {
            let movie = {
                link: 'https://www.youtube.com/watch?v=xAG1uF9O1Is'
            };
            chai.request(server)
                .post('/movie/share')
                .set({ 'Authorization': `${VALID_TOKEN}` })
                .send(movie)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object').that.is.empty;
                    done();
                });
        });
        it('it should not POST a movie with a expired token of contributor', (done) => {
            let movie = {
                link: 'https://www.youtube.com/watch?v=xAG1uF9O1Is'
            };
            chai.request(server)
                .post('/movie/share')
                .set({ 'Authorization': `JWT ${INVALID_TOKIEN}` })
                .send(movie)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object').that.is.empty;
                    done();
                });
        });
        it('it should POST a movie', (done) => {
        	let movie = {
                link: 'https://www.youtube.com/watch?v=xAG1uF9O1Is'
            };
            chai.request(server)
                .post('/movie/share')
                .set({ 'Authorization': `JWT ${VALID_TOKEN}` })
                .send(movie)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('movie');
                    res.body.movie.should.have.property('id');
                    res.body.movie.should.have.property('title');
                    res.body.movie.should.have.property('description');
                    res.body.movie.should.have.property('link');
                    res.body.movie.should.have.property('contributorId');
                    res.body.movie.should.have.property('contributor');
                    res.body.movie.should.have.property('type');
                    done();
                });
        });
    });
    /*
     * Test the /GET list of movie
     */
    describe('/GET list of movie', () => {
        it('it should GET a default list with 11 movies if lastId is empty', (done) => {
            chai.request(server)
                .get('/movie/list')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('movies');
                    res.body.movies.length.should.be.eql(11);
                    done();
                });
        });
        it('it should GET a list of movie that not include lastId', (done) => {
            let lastId = '5da025df561223a2da4df97f';
            chai.request(server)
                .get('/movie/list')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                    res.body.should.have.property('movies');
                    res.body.movies.should.not.include({ _id: lastId });
                    done();
                });
        });
    });
});